using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public NPC npc;
    bool isTalking = false;

    float distance;
    float currResponseTracker;
    public GameObject player;
    public GameObject dialogueUI;
    public Text npcName, npcDialogue, playerResponse;
    void Start()
    {
        dialogueUI.SetActive(false);
    }   

    private void OnMouseOver()
    {
        distance = Vector3.Distance(player.transform.position, this.transform.position);
        if (distance < 2.5f)
        {
            if(Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                currResponseTracker++;
                if(currResponseTracker >= npc.playerDialogue.Length - 1)
                {
                    currResponseTracker = npc.playerDialogue.Length - 1;
                }
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                currResponseTracker--;
                if (currResponseTracker < 0)
                {
                    currResponseTracker = 0;
                }
            }
            if (Input.GetKeyDown(KeyCode.E) && isTalking == false)
            {
                StartConversation();
            }
            else if (Input.GetKeyDown(KeyCode.E) && isTalking == true)
            {
                EndDialogue();
            }

            if(currResponseTracker == 0 && npc.playerDialogue.Length > 0)
            {
                playerResponse.text = npc.playerDialogue[0];
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    npcDialogue.text = npc.dialogue[1];
                }
            }
            else if (currResponseTracker == 1 && npc.playerDialogue.Length > 1)
            {
                playerResponse.text = npc.playerDialogue[1];
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    npcDialogue.text = npc.dialogue[2];
                }
            }
            else if (currResponseTracker == 2 && npc.playerDialogue.Length > 2)
            {
                playerResponse.text = npc.playerDialogue[2];
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    npcDialogue.text = npc.dialogue[3];
                }
            }
        }
    }

    void StartConversation()
    {
        isTalking = true;
        currResponseTracker = 0;
        dialogueUI.SetActive(true);
        npcName.text = npc.name;
        npcDialogue.text = npc.dialogue[0];
        
    }

    void EndDialogue()
    {
        isTalking = false;
        dialogueUI.SetActive(false);
    }

}
